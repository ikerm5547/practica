/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication29;

/**
 *
 * @author Iker Martinez
 */
public class JavaApplication29 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        //contructor por omision
        Terreno terreno = new Terreno();
        
        terreno.setAncho(10.50f);        
        terreno.setLargo(20.00f);
        
        System.out.println("perimetro es igual a:" + terreno.calculadoraPerimetro());
        System.out.println("Area es igual a: "+ terreno.calculadoraArea());
        
        Terreno ter = new Terreno(10,20.20f,40.00f);
        
        System.out.println("lo ancho es: "+ter.getAncho());
        
        Terreno ter2 = new Terreno(terreno);
        System.out.println("perimetro es igual a:" + ter2.calculadoraPerimetro());
        System.out.println("Area es igual a: "+ ter2.calculadoraArea());
        
        
    }
    
}
